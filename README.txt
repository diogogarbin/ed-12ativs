1-Concat: O concat nos permite concatenar dois Arrays, strings ou variáveis.

2-Every: Testa se todos os elementos da matriz passam no teste implementado pela função.

3-Filter: Um filtro, onde temos um Array e pedimos para ele filtrar com os parametros que foram passados.

4-For Each: Ele irá percorrer cada valor de um Array, executando o código que foi passado dentro da função.

5-Index Of: Está função tem o mesmo propósito da função anterior, porém, o last index of conta o index a partir do final, e o index of, conta o index e suas posições a partir do início do Array ou string.

6-Join: No join, ele irá concatenar os valores de um Array. Se não passarmos nada, ele apenas irá juntar todos os 
valores do Array, se passarmos uma vírgula ou um espaço, ele irá juntar tudo e separar os valores por vírgula ou 
espaço.

7-Last Index Of: Temos uma string contendo uma frase, e uma outra string contendo uma palavra que contem na frase. 
Então, utilizamos o lastIndexOf para saber a quantos há quanto está do final da frase.

8-Map: O map gera um novo Array como resultado, onde cada valor será afetado pela função que é passado dentro do map.

9-Reverse: Recebe o Array, e sem mudar a ordem dos valores, os inverte dentro uniformemente.

10-Slice: O slice retorna um novo Array onde podemos cortar os valores passando no slice suas posições.

Some: O método some testa se pelo menos um elemento na matriz passa no teste implementado pela função fornecida.

Sort: O sort ordena os valores por ordem alfabética, ou por ordem crescente se números.



